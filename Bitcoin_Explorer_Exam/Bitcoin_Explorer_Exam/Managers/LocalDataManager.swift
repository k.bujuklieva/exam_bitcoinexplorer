//
//  LocalDataManager.swift
//  Bitcoin_Explorer_Exam
//
//  Created by Kamelia Bujuklieva on 6.11.21.
//

import Foundation
import RealmSwift

class LocalDataManager {
    static let realm = try! Realm(configuration: realmConfiguration, queue: DispatchQueue.main)
    
    static let realmConfiguration: Realm.Configuration = {
        var configuration = Realm.Configuration.defaultConfiguration
        configuration.schemaVersion = 2
        configuration.deleteRealmIfMigrationNeeded = true
        configuration.migrationBlock = { (migration, version) in
            
        }
        
        return configuration
    }()
//    
//    static let accounts: Account = {
//        if let accountObject = realm.object(ofType: Account.self, forPrimaryKey: "1") {
//            return accountObject
//        }
//        
//        let accountObject = Account()
//        accountObject.userId = "1"
//        realm.beginWrite()
//        realm.add(accountObject, update: .all)
//        try? realm.commitWrite()
//        return accountObject
//    }()
}

