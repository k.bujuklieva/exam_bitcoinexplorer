//
//  RequestManager.swift
//  Bitcoin_Explorer_Exam
//
//  Created by Kamelia Bujuklieva on 6.11.21.
//

import Foundation
import Alamofire


struct API {
    static let baseURL = URL(string: "https://blockchain.info/")!
}



enum RequestManagerError: Error {
    case cannotParseData
}

class RequestManager {
    class func fetchSingleAccountData(
        account: String,
        completion: @escaping ((Error?, Account?) -> Void)
    ) {
        
        let reqUrl = API.baseURL.appendingPathComponent("rawaddr/\(account)")
        
        
        AF.request(reqUrl,
                   method: .get)
            .responseJSON { response in
                guard response.error == nil else {
                    completion(response.error, nil)
                    return
                }

                guard let accountJSON = response.value as? [String: Any] else {
                    completion(RequestManagerError.cannotParseData, nil)
                    return
                }

                let accountDetails = Account(value: accountJSON)

                
                DispatchQueue.main.async {
                    completion(nil, accountDetails)
//                    try? LocalDataManager.realm.write {
//                        LocalDataManager.realm.add(accountDetails, update: .all)
//                    }

//                    NotificationCenter.default.post(name: .accountDataLoaded, object: nil)
//                    completion(nil)
                }
            }
    }
    
    class func fetchSingleBlockData(
        block: String,
        completion: @escaping ((Error?, Block?) -> Void)
    ) {
        
        let reqUrl = API.baseURL.appendingPathComponent("rawblock/\(block)")
        
        
        AF.request(reqUrl,
                   method: .get)
            .responseJSON { response in
                guard response.error == nil else {
                    completion(response.error, nil)
                    return
                }

                guard let blockJSON = response.value as? [String: Any] else {
                    completion(RequestManagerError.cannotParseData, nil)
                    return
                }

                let blockDetails = Block(value: blockJSON)

                
                DispatchQueue.main.async {
                    completion(nil, blockDetails)
//                    try? LocalDataManager.realm.write {
//                        LocalDataManager.realm.add(accountDetails, update: .all)
//                    }

//                    NotificationCenter.default.post(name: .accountDataLoaded, object: nil)
//                    completion(nil)
                }
            }
    }
        
    
    
//    class func uploadAccount(account: String, completion: @escaping((_ error: Error?)->Void)) {
//            let accountJson = account.jsonValue
//
//            AF.request("https://softuni-rest-2021-default-rtdb.europe-west1.firebasedatabase.app/users/.json",
//                       method: .post,
//                       parameters: accountJson,
//                       encoding: JSONEncoding.default).responseJSON { result in
//                guard result.error == nil else {
//                    completion(result.error)
//                    return
//                }
//
//                guard let resultValue = result.value else {
//                    completion(NetworkErrors.cannotParseData)
//                    return
//                }
//
//                guard let accountDict = resultValue as? [String: Any] else {
//                    completion(NetworkErrors.cannotParseData)
//                    return
//                }
//
//                print(accountDict)
//
//                completion(nil)
//            }
//        }
//
}
