//
//  NotificationName.swift
//  Bitcoin_Explorer_Exam
//
//  Created by Kamelia Bujuklieva on 6.11.21.
//

import Foundation
extension Notification.Name {
    static let blockDataLoaded = Notification.Name("blockDataLoadedNotification")
}
