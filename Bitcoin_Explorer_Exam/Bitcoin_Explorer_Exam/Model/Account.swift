//
//  Account.swift
//  Bitcoin_Explorer_Exam
//
//  Created by Kamelia Bujuklieva on 6.11.21.
//

import Foundation
import RealmSwift

class Account: Object {
    @Persisted var hash160: String
    @Persisted var address: String
    @Persisted var n_tx: Int
    @Persisted var total_received: Int
    @Persisted var total_sent: Int
    @Persisted var final_balance: Int
}


