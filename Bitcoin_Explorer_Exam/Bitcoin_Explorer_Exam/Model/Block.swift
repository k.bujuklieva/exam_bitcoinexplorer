//
//  BlockModel.swift
//  Bitcoin_Explorer_Exam
//
//  Created by Kamelia Bujuklieva on 6.11.21.
//

import Foundation
import RealmSwift

class Block: Object {
    @Persisted var height: Int
    @Persisted var n_tx: Int
    @Persisted var time: Int
    @Persisted var mrkl_root: String
    @Persisted var nonce: Int
    }
