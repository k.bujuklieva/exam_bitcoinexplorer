//
//  BlockDetailsViewController.swift
//  Bitcoin_Explorer_Exam
//
//  Created by Kamelia Bujuklieva on 6.11.21.
//

import UIKit

class BlockDetailsViewController: UIViewController {

    @IBOutlet weak var blockHashLabel: UILabel!
    @IBOutlet weak var blockHeightLabel: UILabel!
    @IBOutlet weak var blockTimeLabel: UILabel!
    @IBOutlet weak var merkleRootLabel: UILabel!
    @IBOutlet weak var blockTransactionsLabel: UILabel!
    @IBOutlet weak var blockNonceLabel: UILabel!
    
    var blockNum: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
        if let blockNum = blockNum {
            RequestManager.fetchSingleBlockData(
                block: blockNum) { [weak self] error, bl in
                    self?.presentBlockData(bl)
            }
        }
        
    }
    
    func presentBlockData(_ block: Block?) {
        guard let block = block else {
            return
        }

//        blockHashLabel.text = "Hash: \(block.hash)"
        blockHeightLabel.text = "Height: \(block.height)"
        blockTimeLabel.text = "Time: \(block.time)"
        merkleRootLabel.text = "Merkle root: \(block.mrkl_root)"
        blockTransactionsLabel.text = "Transactions: \(block.n_tx)"
        blockNonceLabel.text = "Nonce: \(block.nonce)"
    }
    

}
