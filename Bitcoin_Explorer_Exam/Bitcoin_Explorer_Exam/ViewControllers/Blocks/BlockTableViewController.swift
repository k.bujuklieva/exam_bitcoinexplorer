//
//  ViewController.swift
//  Bitcoin_Explorer_Exam
//
//  Created by Kamelia Bujuklieva on 6.11.21.
//

import UIKit

class BlocksTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    var blockData =
        [("0000000000000bae09a7a393a8acded75aa67e46cb81f7acaa5ad94f9eacd103", "time"),
         ("0000000000000981c0f836cc249fb18744fd33458b85d00de3e7f8995f4543ec", "time2"),
         ("7fec6bd918ee43fddebc9a7d976f3c6d31a61efb4f27482810a6b63f0e4a02d5", "time3"),
         ("a9300383c7b0f5fc03d495844420f25035c34c4c1abb0bdb43fed1d491bbb5e2", "hey4"),
         ("956365e81276bea27acc4278c90481a2c178b402ed988e976e205fb0e28c1ebc", "hey5")
         ]
    
    @IBOutlet weak var blocksTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        blocksTable.dataSource = self
        blocksTable.delegate = self
        // Do any additional setup after loading the view.
    }
    
    //MARK: table view
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.blockData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "blockCell", for: indexPath)
        cell.textLabel?.text = self.blockData[indexPath.row].0
        cell.detailTextLabel?.text = self.blockData[indexPath.row].1
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print(blockData[indexPath.row])
        let currentObject = blockData[indexPath.row]
        performSegue(withIdentifier: "block", sender: currentObject)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if let blockDetailsController = segue.destination as? BlockDetailsViewController,
               let tuple = sender as? (String, String) {
                blockDetailsController.blockNum = tuple.0
            }
    }

}
