//
//  AccountDetailsViewController.swift
//  Bitcoin_Explorer_Exam
//
//  Created by Kamelia Bujuklieva on 6.11.21.
//

import UIKit

class AccountDetailsViewController: UIViewController {

    @IBOutlet weak var blockHashLabel: UILabel!
    @IBOutlet weak var blockAddressLabel: UILabel!
    @IBOutlet weak var transactionsNumberLabel: UILabel!
    @IBOutlet weak var totalSentLabel: UILabel!
    @IBOutlet weak var totalReceivedLabel: UILabel!
    @IBOutlet weak var finalBalanceLabel: UILabel!
    
    var accountNum: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let accountNum = accountNum {
            RequestManager.fetchSingleAccountData(
                account: accountNum) { [weak self] error, acc in
                    self?.presentAccData(acc)
            }
        }
        
    }
    
    func presentAccData(_ account: Account?) {
        guard let account = account else {
            return
        }

        blockHashLabel.text = "Hash: \(account.hash160)"
        blockAddressLabel.text = "Address: \(account.address)"
        transactionsNumberLabel.text = "Transactions: \(account.n_tx)"
        totalSentLabel.text = "Total(sent) \(account.total_sent)"
        totalReceivedLabel.text = "Total(received): \(account.total_received)"
        finalBalanceLabel.text = "Final balance: \(account.final_balance)"
    }
    

}
