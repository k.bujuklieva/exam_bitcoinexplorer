//
//  AddAccountViewController.swift
//  Bitcoin_Explorer_Exam
//
//  Created by Kamelia Bujuklieva on 6.11.21.
//

import UIKit

class AddAccountViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var accountTextField: UITextField!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

        @IBAction func confirmButtonTapped(_ sender: Any) {
            
            guard let accountNum = accountTextField.text else {
                return
            }
            
            RequestManager.fetchSingleAccountData(account: accountNum) { error, account in
                
            }
        
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
