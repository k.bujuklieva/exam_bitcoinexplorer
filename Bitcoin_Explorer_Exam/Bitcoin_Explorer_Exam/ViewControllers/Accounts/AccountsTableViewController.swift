//
//  AccountsTableViewController.swift
//  Bitcoin_Explorer_Exam
//
//  Created by Kamelia Bujuklieva on 6.11.21.
//

import UIKit

class AccountsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
//    var accounts: [Account] = [Account]()
    var accountsData = [("1AJbsFZ64EpEfS5UAjAfcUG8pH8Jn3rn1F", "1233"),
                        ("hey", "hey2"),
                        ("hey", "hey3"),
                        ("hey", "hey4"),
                        ("hey", "hey5")
                         ]
    
    
    @IBOutlet weak var accountsTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        accountsTable.dataSource = self
        accountsTable.delegate = self
    }
    

    @IBAction func didTapAddAccountButton(_ sender: Any) {
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.accountsData.count
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "accountCell", for: indexPath)
        cell.textLabel?.text = self.accountsData[indexPath.row].0
        cell.detailTextLabel?.text = self.accountsData[indexPath.row].1
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print(accountsData[indexPath.row])
        let currentObject = accountsData[indexPath.row]
        performSegue(withIdentifier: "accountDetails", sender: currentObject)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if let accountDetailsController = segue.destination as? AccountDetailsViewController,
               let tuple = sender as? (String, String) {
                accountDetailsController.accountNum = tuple.0
            }
    }
}

